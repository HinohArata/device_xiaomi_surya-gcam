# LMC Gcam for Surya

### How to use?
1. clone this repo use this command
```
git clone https://gitlab.com/HinohArata/device_xiaomi_surya-gcam.git vendor/GoogleCamera
```
2. Add this to `device.mk`
```
# Inherit GoogleCamera apps
$(call inherit-product-if-exists, vendor/GoogleCamera/gcam.mk)
```

