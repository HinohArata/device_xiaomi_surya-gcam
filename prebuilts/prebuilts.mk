# Copy permission files
CAMERA_PATH := vendor/GoogleCamera

PRODUCT_COPY_FILES += \
    $(CAMERA_PATH)/prebuilts/product/etc/permissions/googlecamera-hiddenapi-package-whitelist.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/sysconfig/googlecamera-hiddenapi-package-whitelist.xml \
    $(CAMERA_PATH)/prebuilts/product/etc/permissions/privapp-permissions-com.google.android.GoogleCameraEngR13F2.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/privapp-permissions-com.google.android.GoogleCameraEngR13F2.xml \
    $(CAMERA_PATH)/prebuilts/product/etc/permissions/googlecamera-permissions.xml$:(TARGET_COPY_OUT_SYSTEM)/etc/default-permissions/googlecamera-permissions.xml

# Build apps
PRODUCT_PACKAGES += \
    GoogleCamera